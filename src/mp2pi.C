#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <unistd.h>

#include "mpi.h"
#include "omp.h"

void ReduceTimers(std::vector<double> &processTimes,std::vector<std::vector<double> > &parallelTimes)
{
  unsigned int nTimes = processTimes.size();
  parallelTimes.resize(3);
  parallelTimes[0].resize(nTimes);
  parallelTimes[1].resize(nTimes);
  parallelTimes[2].resize(nTimes);
  MPI_Reduce(&processTimes[0],&(parallelTimes[0][0]),nTimes,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
  MPI_Reduce(&processTimes[0],&(parallelTimes[1][0]),nTimes,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Reduce(&processTimes[0],&(parallelTimes[2][0]),nTimes,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
}

inline double f(double xPos)
{
  // (d/dx)(arctan(x)) = 1/(1+x^2)
  // INTEGRAL[0,1] = PI/4 
  return(1.0/(1.0 + xPos*xPos));
  // 
  // INTEGRAL[0,INF]e^(-x^2) = sqrt(PI)/2
  //  return(std::exp(-xPos*xPos));
}

int main(int argc,char *argv[])
{
  MPI_Init(&argc,&argv);

  int numTimers = 2;
  std::vector<double> mpiTimers(numTimers,0);
  std::vector<std::vector<double> > parallelTimes;

  int procRank = 0;
  int numProcs = 1;
  MPI_Comm_rank(MPI_COMM_WORLD,&procRank);
  MPI_Comm_size(MPI_COMM_WORLD,&numProcs);
  int numThreads = omp_get_max_threads();

  size_t numPoints = 0;
  if(argv[1]){
    std::string numPointsString(argv[1]);
    std::istringstream Istr(numPointsString);
    Istr >> numPoints;
  }
  if(numPoints <= 1) numPoints = 1000;

  bool debugOn = false;
  if(argc > 2)
    debugOn = true;

  if(debugOn){
    for(int j = 0;j < numProcs;j++){
      if(j == procRank){
#pragma omp parallel
	{
	  int threadID   = omp_get_thread_num();
	  for(int i = 0;i < numThreads;i++){
	    if(i == threadID){
	      std::cout << "Hello MP^2 world! Rank(" << procRank 
			<< "," << threadID << ")" << std::endl;
	    }
#pragma omp barrier
	  }
	}  
      }
      MPI_Barrier(MPI_COMM_WORLD);
    }
  }    
  
  MPI_Bcast(&numPoints,1,MPI_LONG_LONG,0,MPI_COMM_WORLD);
  double xMax   = 1.0;
  double deltaX = xMax/static_cast<double>(numPoints-1);
  size_t numLocalPoints = numPoints/numProcs;
  size_t numLocal = numLocalPoints;
  if(numLocalPoints == 0){
    std::cerr << "Too few points (" << numPoints 
	      << ") for " << numProcs 
	      << " processors." << std::endl;
    MPI_Finalize();
    exit(1);
  } else if (numLocalPoints/numThreads == 0){
    std::cerr << "Too few points (" << numLocalPoints 
	      << ") for " << numThreads << " threads." 
	      << std::endl;
    MPI_Finalize();
    exit(1);
  }
  
  size_t numLeftover    = numPoints%numProcs;
  if(procRank < numLeftover)
    numLocalPoints++;
  
  std::vector<double> xData(numLocalPoints,0);
  size_t localStart = 0;
  for(size_t j = 0;j < procRank;j++){
    localStart += numLocal;
    if(j < numLeftover)
      localStart++;
  }
  
  size_t localEnd = localStart + numLocalPoints - 1;
  
  double xPosition = localStart*deltaX;
  
  MPI_Barrier(MPI_COMM_WORLD);

  double initTime = MPI_Wtime();

#pragma omp parallel 
  {
#pragma omp for
    for(size_t i = 0;i < numLocalPoints;i++){
      xData[i] = xPosition + i*deltaX; 
    }
  }

  initTime = MPI_Wtime() - initTime;
  mpiTimers[0] = initTime;


  if(debugOn) {
    // if(procRank == 0){
    //   unlink("XData.txt");
    //   std::ofstream Ouf;
    //   Ouf.open("XData.txt");
    //   Ouf << "# i X f" << std::endl;
    //   Ouf.close();
    // }
    // MPI_Barrier(MPI_COMM_WORLD);
    for(int i = 0;i < numProcs;i++){
      if(i == procRank){
	std::cout << "Rank[" << procRank << "] : I("
		  << localStart << ":" << localEnd << ") X("
		  << xData[0] << ":" << xData[numLocalPoints - 1]
		  << ") Number of Points: " << numLocalPoints << "."
		  << std::endl;
	// std::ofstream Ouf;
	// Ouf.open("XData.txt",std::ios_base::app);
	// for(int j = localStart;j <= localEnd;j++)
	//   Ouf << j << " " << xData[j-localStart] << " "
	//       << f(xData[j-localStart]) << std::endl;
	// Ouf.close();
      }
      MPI_Barrier(MPI_COMM_WORLD);
    }
  }
  
  MPI_Barrier(MPI_COMM_WORLD);

  double processLocalSum = 0.0;
  double threadLocalSum = 0.0;
  
  double computeTime = MPI_Wtime();

#pragma omp parallel 
  {
#pragma omp for reduction(+:processLocalSum)
    for(size_t i = 0;i < numLocalPoints;i++)
      processLocalSum += f(xData[i]);  
  }

  computeTime = MPI_Wtime() - computeTime;
  mpiTimers[1] = computeTime;
  
  // std::string sumsFileName;
  // if(debugOn){
  // if(procRank == 0){
  //   std::ofstream Ouf;
  //   Ouf.open("Sums.txt");
  //   Ouf << "# process sum" << std::endl;
  //   Ouf.close();
  // }
  //   for(int i = 0;i < numProcs;i++){
  //     if(i == procRank){
  // 	std::ostringstream Ostr;
  // 	Ostr << "Sums_" << numProcs << ".txt";
  // 	sumsFileName = Ostr.str();
  // 	std::ofstream Ouf;
  // 	Ouf.open(sumsFileName.c_str(),std::ios_base::app);
  // 	Ouf << i << " " << processLocalSum << std::endl;
  // 	Ouf.close();
  //     }
  //     MPI_Barrier(MPI_COMM_WORLD);
  //   }
  // }
  
  double totalSum = 0;
  MPI_Reduce(&processLocalSum,&totalSum,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  // if(debugOn && procRank == 0){
  //   std::ofstream Ouf;
  //   Ouf.open(sumsFileName.c_str(),std::ios_base::app);
  //   Ouf << "Total: " << totalSum << std::endl;
  //   Ouf.close();
  // }
  
  ReduceTimers(mpiTimers,parallelTimes);

  if(procRank == 0){

    // Average timers 
    for(int i = 0;i < numTimers;i++){
      parallelTimes[2][i] /= static_cast<double>(numProcs);
    }

    double myPI = 3.141592653589793238462643383;
    //    myPI = std::sqrt(myPI)/2;
    totalSum -= .5*(f(0)+f(xMax));
    double expPI = 4.0*totalSum*deltaX;
    double errPI = std::abs(myPI - expPI)/myPI;
    if(debugOn){
      std::cout 
	<< "Real Value: " 
	<< std::setprecision(24) << std::setw(26)
	<< myPI << std::endl << " Exp Value: " 
	<< std::setprecision(24) << std::setw(26)
	<< expPI << std::endl << " Error: "
	<< std::setprecision(5) << std::scientific 
	<< errPI << std::endl;
    }
    std::cout 
      << "initTime(" << std::setprecision(2) 
      << parallelTimes[0][0] << ","
      << parallelTimes[1][0] << "," << parallelTimes[2][0] 
      << ") computeTime(" << parallelTimes[0][1] << "," 
      << parallelTimes[1][1] << "," << parallelTimes[2][1] << ")" 
      << std::endl;
  }
  
  MPI_Finalize();
  return 0;
}
